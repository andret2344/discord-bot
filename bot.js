const Discord = require("discord.js");
const client = new Discord.Client();
const ontime = require('ontime');
const winston = require("winston");
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;
const malbot = require("./Malbot").Malbot;
const fs = require('fs');

const myFormat = printf(info => {
	return `[${info.timestamp}] [${info.level.toUpperCase()}] ${info.message}`;
});

const logger = winston.createLogger({
	level: 'info',
	format: combine(timestamp(), myFormat),
	transports: [
		new winston.transports.File({ filename: 'error.log', level: 'error' }),
		new winston.transports.File({ filename: 'logs.log' }),
		new winston.transports.Console()
	],
	exitOnError: false
});

malbot.setOnStartListener(function() {
	fs.readdir("./commands/", (err, files) => {
		files.forEach(file => {
			const x = require("./commands/" + file);
			malbot.addCommand(x);
			logger.info("Loaded ./commands/" + file);
		});
	});
	fs.readdir("./listeners/", (err, files) => {
		files.forEach(file => {
			const x = require("./listeners/" + file);
			malbot.addListener(x);
			logger.info("Loaded ./listeners/" + file);
		});
	});
	logger.info("Started");
});

client.on("ready", () => malbot.run());

client.on("message", (message) => {
	var channel = message.channel;
	var sender = message.author;
	if (message.content.startsWith("/")) {
		var args = message.content.split(" ");
		var cmd = args[0].substring(1);
		args = args.splice(0, 1);
		logger.info(sender.username + ": " + message.content);
		for (c in malbot.commands) {
			malbot.commands[c](cmd, args, channel, sender);
		}
	} else {
		for (l in malbot.listeners) {
			malbot.listeners[l](client, message);
		}
	}
});

malbot.setOnTime("13:07:50", function() {
	client.channels.get("438060807037648929").send("Codziennie o tej godzinie");
})

ontime({cycle: '7:00:00'}, function(ot) {
	// utils.packtDownload(logger);
	ot.done();
});

ontime({cycle: '22:48:00'}, function(ot) {
	// utils.holiday(config.channels.generalChannelId);
	ot.done();
});

client.login("NDM4MDY4MjUyOTE3MzY2Nzg1.Db_OZA.eNCLrYH5r1lBc5UDoC5ktWFxLeA");