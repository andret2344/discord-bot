# Discord bot

## Running

To run the bot you need to have node installed. You can simply download it. Then just go into into the bot folder and type:

    npm install
    
This should install all necessary dependencies. If not, then install them manually:

	npm install discord.js
	npm install ontime
	npm install winston
	
Once dependencies are installed, run the bot:

	node bot.js

And that's it. The bot is now running. Enjoy!