const ontime = require('ontime');

exports.Malbot = {
	onStartListener: null,
	commands: [],
	listeners: [],

    setOnStartListener: function(onStartListener) {
		this.onStartListener = onStartListener;
	},

	addCommand: function(command) {
		if (command != null) {
			this.commands.push(command);
		}
	},

	addListener: function(listener) {
		if (listener != null) {
			this.listeners.push(listener);
		}
	},

	setOnTime: function(time, onTime) {
		ontime({cycle: time}, function(ot) {
			if (onTime != null) {
				onTime();
			}
			ot.done();
		});
	},

    run: function() {
		if (this.onStartListener != null) {
			this.onStartListener();
		}
	}
}